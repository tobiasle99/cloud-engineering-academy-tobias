from flask import Flask


app = Flask(__name__)

@app.route('/health', methods=['GET'])
def get_health():
    return {
        'status': 'up'
    }

@app.route('/')
def home():
    return "Hello world"

if __name__ == '__main__':
    app.run(debug=True, port=5000)