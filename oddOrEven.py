#! /usr/bin/python3
myNum = int(input("Please enter your number: ")) #waits for user input, converts to integer
if (myNum%2 == 0): #if modulo is equal to 0 => number is even
    for i in range(1, 6):
        result = myNum*i*2
        print(f"{myNum} * {i} * 2 = {result}" )
else:
    print("%d is an odd number, nothing to do here" %(myNum))
#testing git branches pls ignore