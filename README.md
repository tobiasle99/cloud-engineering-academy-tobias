DevOps/Cloud Engineering Academy from Applifting

Showcasing skills that I have learned in this self-study.

1. Git - all of the files in this repo have been pushed through the terminal.
2. bash - showing off a simple for loop in bash
3. python - the same script as in bash, written in python
4. docker - created a dockerfile that builds an image of a flask app
5. kubernetes - using minikube (vm = docker) started a cluster, on which the given flask app runs
6. nginx ingress - created a dns called mojeflaskapp.com and assigned it to the given app
7. prometheus/grafana - implemented a monitoring stack consisting of prometheus and grafana using helm
8. gitlab CI/CD - this "pipeline" tests whether the Dockerfile can build the image on every push
