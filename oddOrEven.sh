#!/usr/bin/bash

echo "Hello! Please enter a number! "
read input

if [ `expr $input % 2` == 0 ]
then
    for x in {2..10..2};
    do
    echo $(expr $x \* $input)
    done
else
    echo "Number $input is odd, nothing to do here."
fi
